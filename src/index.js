import {render} from 'react-dom'
import React, {useState} from 'react'
import {useSprings, animated, interpolate} from 'react-spring'
import {useGesture} from 'react-use-gesture'
import './styles.css'

const PAGES = ['3','2', '1'];

const pageContainer = number => {
    switch (number) {
        case 2:
            return (
                <div className="page">
                    <div className="promo-block-divider">
                        <div className="promo-block-start">
                            <div className="promo-block-boy"/>
                            <div className="promo-block-info">
                                <h1 className="promo-block-title">Andrey <br/> Mironov</h1>
                                <p className="promo-block-text">Full Stack JavaScript Developer &amp; Web Designer</p>
                            </div>
                        </div>
                    </div>
                    <h1>Socials</h1>
                    <ul className="list-inline">
                        <li><a href="#" target="_blank"><i
                            className="fa fa-linkedin fa-3x" aria-hidden="true"/></a></li>
                        <li><a href="#" target="_blank"><i className="fa fa-github fa-3x"
                                                                                         aria-hidden="true"/></a></li>
                        <li><a href="#" target="_blank"><i
                            className="fa fa-twitter fa-3x"
                            aria-hidden="true"/></a></li>
                        <li><a href="#" target="_blank"><i
                            className="fa fa-medium fa-3x"
                            aria-hidden="true"/></a></li>
                    </ul>
                    <h1 className="list-inline-notice">Don't go away fast! :)</h1>
                </div>
            );
        case 0:
            return (
                <div className="page">
                    <div className="info-text">
                        <h2>Coding</h2>
                        <div className="info-text-block">
                            <p>I constantly strive to learn as many programming technologies as possible (JavaScript, C / C ++, C #, Java, PHP), frameworks (React, NodeJS, React Native) in order to become a more well-developed software engineer.</p>
                            <div className="full-with--coding"/>
                        </div>
                        <h2>Hacking</h2>
                        <div className="info-text-block">
                            <div className="full-with--hacking"/>
                            <p>I pay a lot of attention to security issues, I participate in Russian and international CTF competitions.</p>
                        </div>
                        <h2>Design</h2>
                        <div className="info-text-block">
                            <p>In my free time I do web and graphic design. Some work has been implemented in production products...</p>
                            <div className="full-with--design"/>
                        </div>

                    </div>
                </div>
            );
        case 1:
            return (
                <div className="page">
                    <div className="skills">
                        <h2>Skills</h2>
                        <div className="skill-frontend">
                            <div className="skill-frontend-part">
                                <h3>Frontend part I</h3>
                                <p>HTML5, Templating engines, CSS3, CSS frameworks, CSS preprocessors, CSS Grid, Flexbox, Material Design, BEM, Pattern Library.</p>
                            </div>
                            <div className="skill-frontend-part">
                                <h3>Frontend part II</h3>
                                <p>JS, ES6, JS Frameworks (React, Redux, jQuery), TypeScript, JS Tools (Yarn, npm, Webpack, Babel, Browserify, Gulp).</p>
                            </div>
                        </div>
                        <div className="devider"/>
                        <div className="skill-backend">
                            <div className="skill-backend-part">
                                <h3>Backend</h3>
                                <p>Node.js, Express, MongoDB, Firebase, MySQL.</p>
                            </div>
                            <div className="skill-backend-part">
                                <h3>Design</h3>
                                <p>Often Figma, Adobe Photoshop & Illustrator, and also draw on paper and notebook :)</p>
                            </div>
                        </div>
                    </div>
                </div>
            );

    }
};

// These two are just helpers, they curate spring data, values that are later being interpolated into css
const to = i => ({x: 0, y: i * -4, scale: 1, rot: -10 + Math.random() * 20, delay: i * 100});
const from = i => ({x: 0, rot: 0, scale: 1.5, y: -1000});
// This is being used down there in the view, it interpolates rotation and scale into a css transform
const trans = (r, s) => `perspective(1500px) rotateX(10deg) rotateY(${r / 10}deg) rotateZ(${r / 2}deg) scale(${s})`;

function Deck() {
    const [gone] = useState(() => new Set());// The set flags all the cards that are flicked out
    const [props, set] = useSprings(PAGES.length, i => ({...to(i), from: from(i)})); // Create a bunch of springs using the helpers above
    // Create a gesture, we're interested in down-state, delta (current-pos - click-pos), direction and velocity
    const bind = useGesture(({args: [index], down, delta: [xDelta], distance, direction: [xDir], velocity}) => {
        const trigger = velocity > 0.2; // If you flick hard enough it should trigger the card to fly out
        const dir = xDir < 0 ? -1 : 1; // Direction should either point left or right
        if (!down && trigger) gone.add(index);// If button/finger's up and trigger velocity is reached, we flag the card ready to fly out
        set(i => {
            if (index !== i) return; // We're only interested in changing spring-data for the current spring
            const isGone = gone.has(index);
            const x = isGone ? (200 + window.innerWidth) * dir : down ? xDelta : 0; // When a card is gone it flys out left or right, otherwise goes back to zero
            const rot = xDelta / 100 + (isGone ? dir * 10 * velocity : 0);// How much the card tilts, flicking it harder makes it rotate faster
            const scale = down ? 1.1 : 1; // Active cards lift up a bit
            return {x, rot, scale, delay: undefined, config: {friction: 50, tension: down ? 800 : isGone ? 200 : 500}}
        });
        if (!down && gone.size === PAGES.length) setTimeout(() => gone.clear() || set(i => to(i)), 600)
    });
    // Now we're just mapping the animated values to our view, that's it. Btw, this component only renders once. :-)
    return props.map(({x, y, rot, scale}, i) => (
        <animated.div key={i} style={{transform: interpolate([x, y], (x, y) => `translate3d(${x}px,${y}px,0)`)}}>
            {/* This is the card itself, we're binding our gesture to it (and inject its index so we know which is which) */}
            <animated.div {...bind(i)} style={{transform: interpolate([rot, scale], trans)}}>
                <div>{pageContainer(i)}</div>
            </animated.div>
        </animated.div>
    ))
}

render(<Deck/>, document.getElementById('root'));
